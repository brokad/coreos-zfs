FROM ubuntu:17.10 as BUILD
MAINTAINER Damien Broka <damienbroka@mailbox.org>

ARG KERNEL_VERSION
ARG KERNEL_NAME
ARG COREOS_VERSION
ARG ZFS_VERSION
ARG SPL_VERSION

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    	    build-essential \
	    autoconf \
	    libtool \
	    gawk \
	    alien \
	    fakeroot \
	    zlib1g-dev \
	    uuid-dev \
	    libattr1-dev \
	    libblkid-dev \
	    libselinux-dev \
	    libudev-dev \
	    libssl-dev \
	    parted \
	    lsscsi \
	    ksh \
	    git \
	    dh-autoreconf \
	    wget \
	    p7zip-full \
	    ca-certificates \
	    bc

RUN mkdir -p /tmp/build && \
    mkdir /tmp/build/install

WORKDIR /tmp/build/

RUN wget https://alpha.release.core-os.net/amd64-usr/${COREOS_VERSION}/coreos_developer_container.bin.bz2 && \
    bunzip2 coreos_developer_container.bin.bz2

RUN git clone \
    	git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git \
	--single-branch \
	--depth 1 \
	--branch v${KERNEL_VERSION} \
	linux

WORKDIR /tmp/build/linux
RUN 7z e ../coreos_developer_container.bin \
       	 "usr/lib64/modules/*-coreos*/build/.config" && \
    yes "" | make oldconfig && \
    sed -i -e "s/CONFIG_MODULE_SIG=y/CONFIG_MODULE_SIG=n/" \
    	      .config && \
    make modules_prepare && \
    make -j$(nproc) && \
    sed -i -e "s/${KERNEL_VERSION}[+]*/${KERNEL_NAME}/" \
	      include/generated/utsrelease.h

WORKDIR /tmp/build/
RUN git clone https://github.com/zfsonlinux/spl
WORKDIR /tmp/build/spl
RUN git checkout spl-${SPL_VERSION} && \
    sh autogen.sh --with-linux=/tmp/build/linux/ \
       		  --with-linux-obj=/tmp/build/linux/ && \
    ./configure --with-linux=/tmp/build/linux/ \
		--with-linux-obj=/tmp/build/linux/ && \
    make -s -j$(nproc) && \
    make install

WORKDIR /tmp/build/
RUN git clone https://github.com/zfsonlinux/zfs
WORKDIR /tmp/build/zfs
RUN git checkout zfs-${ZFS_VERSION} && \
    sh autogen.sh --with-linux=/tmp/build/linux/ \
       		  --with-linux-obj=/tmp/build/linux/ && \
    ./configure --with-linux=/tmp/build/linux/ \
		--with-linux-obj=/tmp/build/linux/ && \
    make -s -j$(nproc) && \
    make install

FROM ubuntu:17.10
MAINTAINER Damien Broka <damienbroka@mailbox.org>

ARG KERNEL_NAME
ARG KERNEL_VERSION
ARG COREOS_VERSION

ENV KERNEL_NAME ${KERNEL_NAME}
ENV KERNEL_VERSION ${KERNEL_VERSION}
ENV COREOS_VERSION ${COREOS_VERSION}

ENV ZFS_PATH /opt/zfs
ENV ZFS_SBIN_PATH $ZFS_PATH/sbin
ENV ZFS_LIB_PATH $ZFS_PATH/lib

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        module-init-tools \
	udev \
	cryptsetup-bin && \
    apt-get autoremove && \
    apt-get clean

COPY --from=BUILD /lib/modules/${KERNEL_NAME} /lib/modules/${KERNEL_NAME}
COPY --from=BUILD /usr/local/sbin/* $ZFS_SBIN_PATH/
#COPY --from=BUILD /usr/lib/systemd/system/* /usr/lib/systemd/system/
COPY --from=BUILD /usr/local/lib/* $ZFS_LIB_PATH/

VOLUME $ZFS_PATH
ENV PATH $PATH:$ZFS_SBIN_PATH
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:$ZFS_LIB_PATH
COPY setup-zfs-env.sh $ZFS_PATH/setup-zfs-env.sh

CMD insmod /lib/modules/${KERNEL_NAME}/extra/spl/spl.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/nvpair/znvpair.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/avl/zavl.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/icp/icp.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/splat/splat.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/unicode/zunicode.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/zcommon/zcommon.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/zfs/zfs.ko && \
    insmod /lib/modules/${KERNEL_NAME}/extra/zpios/zpios.ko && \
    cp -R $ZFS_PATH /rootfs/opt/zfs
