KERNEL_VERSION = 4.13.9
COREOS_VERSION = 1576.1.0
KERNEL_NAME = $(KERNEL_VERSION)-coreos
SPL_VERSION = 0.7.2
ZFS_VERSION = 0.7.2

all:
	sudo docker build --build-arg KERNEL_VERSION=$(KERNEL_VERSION) \
			  --build-arg KERNEL_NAME=$(KERNEL_NAME) \
			  --build-arg COREOS_VERSION=$(COREOS_VERSION) \
			  --build-arg ZFS_VERSION=$(ZFS_VERSION) \
			  --build-arg SPL_VERSION=$(SPL_VERSION) \
			  -t brokad/coreos-zfs:$(COREOS_VERSION) -f Dockerfile .
